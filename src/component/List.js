import React, { useRef, useState } from 'react';
import listData from '../listData';
import ListItem from './ListItem';
import {
    Col,
    Button,
    Input,
} from 'reactstrap'




const List = ({listName, listkey}) => {

    const [item, setItem] = useState({ title: '', desp:'' })
    const [listItems, setListItems] = useState([])
    const [disabled, setDisabled] = useState(true)
    const [i, setI] = useState(0)
    



    const onChangeHandler = (name,e) => {

        const _value = e.target.value;

        setItem({
            ...item,
            [name]:_value
        })

        if (item.title == '' || item.desp == '') {
            setDisabled(true)
        }else{
            setDisabled(false)
        }
    }



    const fieldHandler = (e, id) => {
        const { value, name } = e.target;   
        

        const _listItems = listItems.map((item) => {

            if(item.id == id) {
                return {
                    ...item,
                    [name]: value
                }
            }
            return item;
        })

        setListItems(_listItems)
    }



    const cardHandler = id => {

        const _listItems = listItems.filter((item) => (
            item.id != id
        ))

        setListItems(_listItems)
    }


    const submitHandler = (i) => {

        if (item.title == '' || item.desp == '') {
        
            setDisabled(true)
        
        }else{
        
            const _currentItem = {
                ...item,
                id:i
            }
    
            setListItems([
                ...listItems,
                _currentItem,
                
            ])
    
    
            setI(++i);
            setItem({ title: '', desp: '' });
            setDisabled(true)
        }
        
        
    }




    return (

        <Col md={3} key={listkey}>
            <div>
                <h3>{listName}</h3>
                {

                    listItems.map((item) => (
                        <ListItem
                            id={item.id}
                            title={item.title}
                            desp={item.desp}
                            listData={listItems}
                            listDataHandler={setListItems}
                            onChangeHandler={fieldHandler}
                            deleteCard={cardHandler}
                        />
                    ))
                }


                <div>
                    <Input 
                        type="text" 
                        name="title" 
                        id="title" 
                        placeholder="Enter Title"
                        className="mb-2"
                        value={item.title}
                        onChange={(e) => onChangeHandler('title',e)}

                    />

                    <Input 
                        type="textarea" 
                        name="desp" 
                        id="desp" 
                        placeholder="Enter Description"
                        className="mb-2"
                        value={item.desp}
                        onChange={(e) => onChangeHandler('desp',e)}
                    />

                    <Button 
                        className="btn-sm"
                        onClick={() => submitHandler(i)}
                        disabled={disabled}
                    >
                        Add New Item
                    </Button>
                </div>
            </div>
        </Col>
       
        
    )
};


export default List;