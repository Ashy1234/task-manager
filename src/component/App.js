import React, { useState } from 'react'
import List from './List'
import AddList from './AddList';
import {
  Container,
  Row,
} from 'reactstrap'



const App = () => {

  const[allList, setAllList] = useState([]);


  return (
    
    <Container>
      
      <Row>
        <AddList 
          allList={allList} 
          listHandler={(item) => setAllList([...allList, item])}
        />
      </Row>

      <Row>

        {
          allList.map((item, id) => (
            <List listName={item.title} key={id} />
          ))
        }
      </Row>

    </Container>
  );

}

export default App;
