import React, { useState } from 'react';
import { 
    Col,
    Button,
    InputGroup,
    InputGroupAddon,
    Input
} from 'reactstrap';



const AddList = ({listHandler, allList}) => {

    const[listName, setListName] = useState('');
    const[disabled, setDisabled] = useState(true)

    const listNameHandler = e => {
        setListName(e.target.value)
    
        if(listName == '') {
            setDisabled(true)

        }else{

            setDisabled(false)
    
        }
    }

    const creatList = () => {

        if(listName == '') {
            setDisabled(true)

        }else{

            
            const _item = {
                title:listName
            }
            
            listHandler(_item)
            setListName('')
            setDisabled(true)
        }
    }
    return(
        <Col md={12} className="my-4">
            <InputGroup>
                <Input
                    type="text"
                    value={listName}
                    placeholder="Enter List Name"
                    onChange={(e) => listNameHandler(e)}
                />
    
                <InputGroupAddon addonType="append">
                    <Button 
                        onClick={creatList}
                        disabled={disabled}
                    >
                        Add new list
                    </Button>
                </InputGroupAddon>
               
            </InputGroup>
        </Col>
    )
}


export default AddList;