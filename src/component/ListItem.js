import React, {useState} from 'react';
import {
    Card,
    CardBody,
    CardTitle,
    CardText,
    Input,
    Dropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
} from 'reactstrap'





const ListItem = (props) => {

    const [dropdownOpen, setDropdownOpen] = useState(false);
    const [disabled, setDisabled] = useState(true)


    const toggle = () => setDropdownOpen(!dropdownOpen);





    return(
            <Card className="mb-3 py-2" key={props.id}>

                <Dropdown isOpen={dropdownOpen} toggle={toggle} size="sm" className="mb-2 mr-2 ml-auto">
                    <DropdownToggle caret>
                        action
                    </DropdownToggle>
                    <DropdownMenu>
                        <DropdownItem onClick={()=> setDisabled(false)}>
                            edit
                        </DropdownItem>

                        <DropdownItem onClick={() => props.deleteCard(props.id)}>
                            delete</DropdownItem>
                    </DropdownMenu>
                </Dropdown>

                <CardBody className="p-0 px-2">
                    <CardTitle tag="h4">
                        <Input
                            type="text" 
                            value={props.title}
                            name="title"
                            onBlur={() => setDisabled(true)}
                            disabled={disabled}
                            onChange={(e) => props.onChangeHandler(e,props.id)}
                        />
                    </CardTitle>
                    
                    <CardText>
                        <Input
                            type="textarea" 
                            value={props.desp}
                            name="desp"
                            onBlur={() => setDisabled(true)}
                            disabled={disabled}
                            onChange={(e) => props.onChangeHandler(e,props.id)}

                        />
                    </CardText>
                </CardBody>
            </Card>
    )
}


export default ListItem;